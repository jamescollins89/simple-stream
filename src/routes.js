import Home from './components/Home.vue';
import Video from './components/video/Video.vue';

export const routes = [
    { path: '', name: 'home', component: Home },
    { path: '/video/:videoTitle', name: 'video', component: Video, props: true },
    { path: '/*', redirect: { name: 'home' } }
]